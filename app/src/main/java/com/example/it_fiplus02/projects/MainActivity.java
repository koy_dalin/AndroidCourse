package com.example.it_fiplus02.projects;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.it_fiplus02.projects.DataStore.Logger;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Logger.print("On Create");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Logger.print("On Start");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Logger.print("On Resume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Logger.print("On Pause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Logger.print("On Stop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logger.print("On Destroy");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Logger.print("On SaveInstanceState");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Logger.print("On RestoreInstanceState");
    }
}
